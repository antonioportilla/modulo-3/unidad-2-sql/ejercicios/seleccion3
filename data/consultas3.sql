﻿USE ciclistas;

-- 1.3 Listar el dorsdal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32.
SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32;

-- 1.4 Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32.
SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32;


-- 1.8 Listar el nombre de los puertos cuya altura entre 1000 y 2000 que la altura sea mayorque 2400.

SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura >2400;

-- 1.12 Listar el número de ciclistas que hayan ganado algún puerto.
SELECT count(DISTINCT dorsal) nciclistas FROM puerto;


-- 1.14 Indicar la altura media de los puertos.
SELECT AVG(altura) FROM puerto;
  

-- 1.18 Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot.

SELECT dorsal, código, COUNT(*) AS numero FROM lleva GROUP BY dorsal, código;