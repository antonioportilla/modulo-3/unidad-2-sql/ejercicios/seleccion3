<div>
    <h1>Listado de todos los registros</h1>
    
</div>

<?php
use yii\widgets\ListView;
?>
<table border="5">
    <thead>
        <tr>
            <th>Dorsal</th>
            <th>Código</th>
            <th>Número veces</th>
        </tr>
    </thead>
      <tbody>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
]);
?>
  
    </tbody>
</table>


