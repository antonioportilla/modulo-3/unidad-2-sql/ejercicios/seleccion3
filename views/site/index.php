<?php
use yii\bootstrap\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>consultas de seleccion 3</h1>
Application
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            
             <!-- tercera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 3</h3>
                      <p>Listar el dorsdal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
             
              <!-- cuarta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 4</h3>
                      <p>Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- octava consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 8</h3>
                      <p>Listar el nombre de los puertos cuya altura entre 1000 y 2000 que la altura sea mayorque 2400.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta8a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta8'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- duodecima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 12</h3>
                      <p>Listar el número de ciclistas que hayan ganado algún puerto.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta12a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta12'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
                            <!-- catorce  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 14</h3>
                      <p>Listar el número de ciclistas que hayan ganado algún puerto.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta14a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta14'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
                            
                            <!-- dieciocho  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 18</h3>
                      <p>Listar el número de ciclistas que hayan ganado algún puerto.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta18a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta18'],['class' => 'btn btn-default btn-large']);?>
                          <?= Html::a('ListView',['site/consulta18b'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>

              
        </div>

    </div>
</div>
