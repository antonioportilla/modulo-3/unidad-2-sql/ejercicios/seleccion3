<?php

namespace app\controllers;
use yii\data\SqlDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /*
     * Creado esta accion
     */
    
    public function actionCrud(){
        return $this->render('gestion');
    }
    
     
    public function actionConsulta3a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("dorsal")->where("nomequipo='Banesto' AND edad BETWEEN 25 AND 32"),
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"Listar el dorsdal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32.",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32",
        ]);
    }
    
     public function actionConsulta3(){
            // mediante DAO
               
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32" ,
                'pagination'=>[
                'pageSize'=>0,
                ],
            
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"Listar el dorsdal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32.",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32",
        ]); 
    }
    
    public function actionConsulta4a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("dorsal")
                ->where("nomequipo='Banesto' OR edad BETWEEN 25 AND 32"),
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32.",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
        ]);
    }
    
     public function actionConsulta4(){
            // mediante DAO
        
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32" ,
        
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32.",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32;",
        ]);
    }
    
      
    
    public function actionConsulta8a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("nompuerto")->where ("altura BETWEEN 1000 AND 2000 OR altura >2400")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Listar el nombre de los puertos cuya altura entre 1000 y 2000 que la altura sea mayorque 2400.",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura >2400",
        ]);
    }
    
     public function actionConsulta8(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura >2400) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura >2400" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Listar el nombre de los puertos cuya altura entre 1000 y 2000 que la altura sea mayorque 2400.",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura >2400",
        ]);
    } 
    
    public function actionConsulta12a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("count(distinct dorsal) as nciclistas")   
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"Listar el número de ciclistas que hayan ganado algún puerto.",
            "sql"=>"SELECT count(DISTINCT dorsal) nciclistas FROM puerto",
        ]);
    }
    
     public function actionConsulta12(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT 1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT count(DISTINCT dorsal) nciclistas FROM puerto" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"Listar el número de ciclistas que hayan ganado algún puerto.",
            "sql"=>"SELECT count(DISTINCT dorsal) nciclistas FROM puerto",
        ]);
    } 
    
     public function actionConsulta14a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("AVG(altura) as maltura")
                ,
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['maltura'],
            "titulo"=>"Consulta 14",
            "enunciado"=>"Indicar la altura media de los puertos.",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
    }
    
     public function actionConsulta14(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT  1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT AVG(altura) AS maltura FROM puerto" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['maltura'],
            "titulo"=>"Consulta 14",
            "enunciado"=>"Indicar la altura media de los puertos.",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
    }
    
    public function actionConsulta18a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Lleva::find()->select("dorsal, código, COUNT(*) numero")->groupBy("dorsal, código")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','numero'],
            "titulo"=>"Consulta 18",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot.   ",
            "sql"=>"SELECT dorsal, código, COUNT(*) AS numero FROM lleva GROUP BY dorsal, código",
        ]);
    }
    
     public function actionConsulta18(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT dorsal, código, COUNT(*) AS numero FROM lleva GROUP BY dorsal, código) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal, código, COUNT(*) AS numero FROM lleva GROUP BY dorsal, código" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','numero'],
            "titulo"=>"Consulta 18",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot.",
            "sql"=>"SELECT dorsal, código, COUNT(*) AS numero FROM lleva GROUP BY dorsal, código",
        ]);
    }
    
    
      public function actionConsulta18b(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()->select("dorsal, código, COUNT(*) numero")->groupBy("dorsal, código"),
            'pagination'=>array (
                'pageSize'=>6,
            ),
        ]);
        
        return $this->render("listView",[
           "dataProvider" => $dataProvider,
        ]);
        
        
    }
    
     }
